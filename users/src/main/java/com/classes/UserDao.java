package com.classes;

import java.util.*;

public class UserDao implements Dao<User> {
    
    private List<User> users = new ArrayList<>();

    public UserDao() {
        users.add(new User("Player1", "test1"));
        users.add(new User("Player2", "test2"));
    }
    
    @Override
    public Optional<User> get(String id) {
        for (User u : users) {
            if (u.getLogin().equals(id)) {
                return Optional.ofNullable(u);
            }
        }
        return Optional.empty();
    }
    
    @Override
    public Set<String> getAll() {
        Set<String> set = new HashSet<>();
        for (User u : users) {
            set.add(u.getLogin());
        }
        return set;
    }
    
    @Override
    public void save(User user) {
        users.add(user);
    }
    
    @Override
    public void update(User user, String[] params) {
    }
    
    @Override
    public void delete(User user) {
        users.remove(user);
    }
}