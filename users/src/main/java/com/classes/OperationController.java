package com.classes;

import com.utils.PresenceUcblJwtHelper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.Console;
import java.util.Optional;

@Controller
public class OperationController {

    // TODO récupérer le DAO...
    ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
    UserDao userDao = ctx.getBean(UserDao.class);
    /**
     * Procédure de login "simple" d'un utilisateur
     * @param login Le login de l'utilisateur. L'utilisateur doit avoir été créé préalablement et son login doit être présent dans le DAO.
     * @param password Le password à vérifier.
     * @return Une ResponseEntity avec le JWT dans le header "Authentication" si le login s'est bien passé, et le code de statut approprié (204, 401 ou 404).
     */
    @PostMapping("/login")
    public ResponseEntity<Void> login(@RequestParam("login") String login, @RequestParam("password") String password, @RequestHeader("Origin") String origin) {
        // TODO
        System.out.println("test");
        Optional<User> u = userDao.get(login);
        if (u.isEmpty()) {
            return new ResponseEntity<Void>(HttpStatus.OK);
        } else {
            try {
                u.get().authenticate(password);
                if(u.get().isConnected() == (true)){
                    String token = PresenceUcblJwtHelper.generateToken(login,origin);
                    HttpHeaders headers = new HttpHeaders();
                    headers.add("Authentification", token);
                    return new ResponseEntity<Void>(headers, HttpStatus.OK);
                } else {
                    // 401
                }
            } catch (Exception e) {

            }
        }

        return null;
    }

    /**
     * Réalise la déconnexion
     */
    @DeleteMapping("/logout")
    // TODO

    /**
     * Méthode destinée au serveur Node pour valider l'authentification d'un utilisateur.
     * @param token Le token JWT qui se trouve dans le header "Authentication" de la requête
     * @param origin L'origine de la requête (pour la comparer avec celle du client, stockée dans le token JWT)
     * @return Une réponse vide avec un code de statut approprié (204, 400, 401).
     */
    @GetMapping("/authenticate")
    public ResponseEntity<Void> authenticate(@RequestParam("token") String token, @RequestParam("origin") String origin) {
        // TODO
        return null;
    }
}
